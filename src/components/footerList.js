import React from 'react';
import FooterItem from './footerItem'

export default function FooterList({items}){
	   // Object.keys(items).map((e, i) => {
    //   <footerItem key={i} {...e} />
    // });
	const navElements =  Object.values(items).map((item,i) => {
       return   <FooterItem key={i} links={item} />
    })
	return (
			<div className="footer-navlist">
					{navElements}
			</div>

	)
	

}