import React, { Component } from 'react';
import Introslide from './Introslide'
class Intro extends Component {
	state={
		slisesMoreThanOne: false,
		curslide: null,
		isAnimating: false
	}
	componentDidMount(){
		const slideslen = Object.keys(this.props.gallery).length;
		if(slideslen > 1){
			this.setState({
				slisesMoreThanOne: true,
			})
		}
		this.setState({
			slidesLength: slideslen,
		})
		setTimeout(()=>{
			this.setState({
				curslide: 1,
			})	
		},100)
	}
	render(props){

		const {gallery} = this.props;
		const list = Object.values(gallery).map((item,i) => {
		    return <Introslide  
			    				defaultActive = {i + 1 === this.state.curslide}
			    				key={i} 
			    				content={item}
		    				/>
    })
    const arrows = this.state.slisesMoreThanOne ? <div className='intro__nav'>
				<button className='intro__navitem' type='button' onClick={this.handleClick.bind(this, 'prev')}>Prev</button>
				<button className='intro__navitem' type='button' onClick={this.handleClick.bind(this, 'next')}>Next</button>
			</div> : null;

		return (
			<div className='intro'>
				<div className='intro__container'>
					<div className='intro__slideswrap'>
						{list}
					</div>
						{arrows}
				</div>
			</div>
		)
	}
	handleClick = (direction) =>{
		if(!this.state.isAnimating){
			this.setState({
				isAnimating: true
			})
			let nextSlide = this.state.curslide + 1;
			let prevSlide = this.state.curslide - 1;
			if(direction === 'next' && nextSlide < this.state.slidesLength + 1){
				this.setState({
					curslide: nextSlide
				})
			}
			else if(direction === 'next' && nextSlide >= this.state.slidesLength + 1){
				this.setState({
					curslide: 1
				})
			}
			if(direction === 'prev' && prevSlide >= 1){
				this.setState({
					curslide: prevSlide
				})
			}else if(direction === 'prev' && prevSlide < 1){
				this.setState({
					curslide: this.state.slidesLength
				})
			}
			setTimeout(()=>{
				this.setState({
					isAnimating: false
				})
			},1200)
		}
	}
}

export default Intro