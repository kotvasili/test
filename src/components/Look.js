import React, { Component } from 'react';

class Look extends Component {
	state={
	}

	render(props){
		const looks = this.props;
		const cards = Object.values(looks).map((look,i) => {
			return (
					<div key={i} className='look__item look-item'>
						<div className='look-item__inner'>
							<div className='look-item__center'>
								<div className='look-item__img'>
									<img src={look.piture}/>
								</div>
								<div className='look-item__content'>
									{/*прости господи*/}
									<h3 className='look-item__title'>
									<b>{look.name} </b>lookbook</h3>
									<p className='look-item__text'>{look.description}</p>
									<a href={look.link} className='btn look-item__button'>view now</a>
								</div>
							</div>
						</div>
					</div>
				)

    });
		return (
				<div className='look'>
					<div className='look__container'>
						{cards}
					</div>
				</div>
		)

	}
}

export default Look