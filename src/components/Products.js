import React, { Component } from 'react';
import Product from './Product';

class Products extends Component {
	state={
		products: this.props.goods,
		filter: null,
		productsToFilter: this.props.goods

	}
	handlleClick(filter){
		if(filter === this.state.filter){
			this.setState({
				filter: null,
				productsToFilter: this.props.goods
			})
		}else{
			this.setState({
				filter: filter,
				productsToFilter: Object.values(this.state.products).filter(good => good.type === filter)
			})
		}
	}
	render(props){
		const {filters} = this.props;
		const filterList = Object.values(filters).map((filter,i) => {
			return <button key={i} type='button' className={'products__filter-item ' +(this.state.filter === filter.link ? 'products__filter-item--active' : '')} onClick={this.handlleClick.bind(this, filter.link)}>{filter.name}</button>
    })
		const cards = Object.values(this.state.productsToFilter).map((product,i) => {
			return <Product key={i} {...product}/>
    })
		return (
			<div className='products'>
				<div className='container'>
					<div className='products__filter'>
							<div className='products__filter-inner'>
								{filterList}
							</div>
					</div>
					<div className='products__grid'>
								{cards}
					</div>
				</div>
			</div>
		)

	}
}

export default Products