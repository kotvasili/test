import React, { Component } from 'react';

class FooterItem extends Component {
	// state={
	// 	isActive: false,
	// }
	constructor(props){
		super(props)

	}

	render(...props){
		const navitam = this.props.links;
		const list =  Object.values(navitam).map((item,i) => {
			      return <li key={i} ><a className={"footer-navlist__link " + (i === 0 ? 'footer-navlist__link--primary' : '')} href={item.link}>{item.name}</a></li>
	    })

		return (
			<ul className="footer-navlist__item">
				{list}
			</ul>
		)
	}

}

export default FooterItem