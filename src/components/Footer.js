import React, { Component } from 'react';
import FooterList from './footerList'
import Contact from './Contact'

class Footer extends Component {
	state={
		// isOpen: true,
	}

	render(props){
		const footer = this.props.footerlinks;

		return (
			<footer className='page__footer'>
				<div className='footer-container'>
					<div className='container'>

					<div className= "footer__nav" >
						<FooterList items={footer.menu}/>

					</div>
					<div className= "footer__contact" >
						<Contact items={footer.contacts}/>
					</div>
				</div>	
				</div>

				<div className='footer-footer'>
					<div className='container'>
							<div className='footer-footer__link footer-footer__link--copy'>{footer.copyright}</div>
							<div className='footer-footer__link'>Design by 
									<a	href='#'>{footer.design}</a>
							</div>
							<div className='footer-footer__link'>Dev by 
									<a	href='#'>{footer.developers}</a>
							</div>
					</div>	
				</div>	
			</footer>
		)

	}
	// handleClick = () =>{
	// 	this.setState({
	// 		isOpen: !this.state.isOpen
	// 	})
	// }
}

export default Footer