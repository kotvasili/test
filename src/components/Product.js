import React, { Component } from 'react';

class Product extends Component {
	state={
		// isOpen: true,
	}

	render(props){
		const product = this.props;
		return (
				<div className={'products__card product '+(product.primary ? 'primary':'')}>
					<div className='product__inner'>
						<a href='#' className='product__link'>
							<div className='product__img'>
								{product.sale !== null ? (
										<div className='product__price'>
											<span className='product__price-item'>
												<sup>£</sup>{product.price}
											</span> 
											<span className='product__price-item'>
												<sup>£</sup>{product.sale}
											</span>
										</div>
									) : (
										<div className='product__price'>
											<span className='product__price-item'>
												<sup>£</sup>{product.price}
											</span>
										</div>
									)
								}
								<img src={product.picture}/>
							</div>
							<div className='product__content'>
								<div className='product__title'>
									{product.name}
								</div>
									{product.description !== null &&
										<p className='product__descr'>
											{product.description}
										</p>
									}
							</div>
						</a>
					</div>
				</div>
		)

	}
}

export default Product