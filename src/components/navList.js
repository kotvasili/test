import React from 'react';
import Navitem from './NavItem'
export default function NavList({navitems}){

	const navElements =  navitems.map((item,index) => 
				<li key={index}><Navitem navdata={item}/></li>
	);

	return (
		<ul>
				{navElements}
		</ul>
	)
	

}
