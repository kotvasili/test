import React, { Component } from 'react';
import Intro from './Intro'
import Products from './Products'
import Look from './Look'
class Main extends Component {
	state={
		// isOpen: true,
	}

	render(props){
		const {intro,collections,books} = this.props.data;
		return (
			<main className='page__content'>
					<Intro {...intro}/>
					<Products {...collections}/>
					<Look {...books}/>
					
			</main>
		)

	}
	// handleClick = () =>{
	// 	this.setState({
	// 		isOpen: !this.state.isOpen
	// 	})
	// }
}

export default Main