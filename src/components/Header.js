import React, { Component } from 'react';
import NavList from './navList'
class Header extends Component {
	state={
		isFixed: false,
	}
	componentDidMount(){
		this.fixScroll()
	}
	fixScroll(){
	  const scrollDelta = 50;
	    
	  let scrolling = false,
	    	currentTop = 0;

	  window.addEventListener('scroll',() => {
	    if( !scrolling ) {
	      scrolling = true;
	      (!window.requestAnimationFrame)
	        ? setTimeout(autoHideHeader, 250)
	        : requestAnimationFrame(autoHideHeader);
	    }
	  });

	  const autoHideHeader = () => {
	    currentTop = window.pageYOffset;
	    checkSimpleNavigation(currentTop);
	    scrolling = false;
	  }

	  const checkSimpleNavigation = (currentTop) => {
	    if(currentTop > scrollDelta) {
	      this.setState({
					isFixed: true,
	      });
	    }else{
	      this.setState({
					isFixed: false,
	      });
	    }
	  }
	}
	render(props){
		const {navitems} = this.props.headerlinks.menu;

		return (
			<header className={'page__header ' +(this.state.isFixed ? 'is-fixed' : '')}>
				<div className='header-inner'>
					<a href='#' className='Logo'>
					<span>
						<b>Avenue</b> Fashion
					</span>
						
					</a>
					<nav className='header__nav'>
						<NavList navitems = { this.props.headerlinks.menu }/>
					</nav>
				</div>
			</header>
		)
	}
}

export default Header