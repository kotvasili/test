import React, { Component } from 'react';

class Introslide extends Component {
	state={
		isActive: false,
	}
  componentWillReceiveProps(nextProps) {
  	// console.log(nextProps)
  }
	render(props){
		const content = this.props.content;
		return (
			<div className={'intro__slide ' + (this.props.defaultActive ? 'intro__slide--active': '')}>
				<div className='intro__slide-inner'>
					<div className='intro__slide-img'>
						<img src={content.picture}/>
					</div>
					<div className='intro__slide-title'>
							<div className='intro__slide-text'>{content.title}</div>
					</div>
				</div>
			</div>
		)

	}
	// handleClick = () =>{
	// 	this.setState({
	// 		isOpen: !this.state.isOpen
	// 	})
	// }
}

export default Introslide