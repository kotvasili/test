import React, { Component } from 'react';

class Contact extends Component {
	state={
		isActive: false,
	}


	render(props){
		const contacts = this.props.items;
		return (
			<div className='contacts'>
				<div className='contacts__head'>
					<a href={contacts.head.link}>{contacts.head.name}</a>
				</div>
				<div className='contacts__body'>
					<p className='contacts__text'>Head office:  {contacts.body.location}</p>
					<p className='contacts__text'> Telephobe:  
						<a href={"tel:" + contacts.body.phone}> {contacts.body.phone}</a>
					</p>
					<p className='contacts__text'> Email: 
						<a href={"mailto:" + contacts.body.mail}> {contacts.body.mail}</a>
					</p>
				</div>
			</div>
			// <a href={this.props.navdata.link}>{this.props.navdata.name}</a>
		)

	}

}

export default Contact