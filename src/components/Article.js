import React, { Component } from 'react';
// import 
class Article extends Component {
	state={
		isOpen: true,
	}

	render(props){
		const {Article} = this.props;
		const body = this.state.isOpen ? <section>body</section>: null;
		return (
			<div className='Article'>
			{body}
			<button className='btn-trigger' onClick={this.handleClick}>{this.state.isOpen?'close':'open'}</button>
			</div>
		)

	}
	handleClick = () =>{
		this.setState({
			isOpen: !this.state.isOpen
		})
	}
}

export default Article