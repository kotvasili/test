import React, { Component } from 'react';
// import Article from './components/Article'
import Header from './components/Header'
import Footer from './components/Footer'
import Main from './components/Main'
// import articleList from './articleList'
import logo from './logo.svg';
import './css/App.css';

class App extends Component {
  state = {
    dataloaded: false,
    data: 'false'
  }
  componentDidMount() {
      this._getData();
  }
 _getData = () => {
    fetch("data.json")
      .then(response => {
          if (response) {
             return response;
           } else {
             let errorMessage = new Error(errorMessage);
              throw(errorMessage);
            }
          })
          .then(response => response.json())
          .then(json =>{
             this.setState({ 
              data: json.page,
              dataloaded: true
            })
          });
        }

  render() {
    if(this.state.dataloaded === false){
       return null
    }
    else{
    const headerlinks = this.state.data.header;
    const footerlinks = this.state.data.footer;
    const rendredTemplate = (
      <div className="page__wrapper">
        <Header headerlinks={headerlinks}/>
        <Main data={this.state.data}/>
        <Footer footerlinks={footerlinks}/>
        {/*<Article />*/}
      </div>
    );
    return rendredTemplate     
    }

    
  }
}

export default App;
